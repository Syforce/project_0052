"use strict";

angular.module("EFL_Project")
	.directive("eflTimer", function($timeout, timerConfig) {
		return {
			restrict: "A",
			templateUrl: "./app/views/directives/timer.tpl.html",
			scope: {
				"timeout": "=",
				"time": "="
			},
			link: function($scope, $element, $attrs) {
				var colors = {
					normal: "green",
					warning: "yellow",
					danger: "red"
				};

				/**
				 * Change the color of the timer
				 */
				function updateColor() {
					if ($scope.time.seconds < timerConfig.DANGER_SECONDS) {
						$scope.color = colors.danger;
					} else if ($scope.time.seconds < timerConfig.WARNING_SECONDS) {
						$scope.color = colors.warning;
					} else if ($scope.time.seconds < timerConfig.MAX_SECONDS) {
						$scope.color = colors.normal;
					}

					$timeout();
				}

				/**
				 * Change the position of the timer
				 */
				function updatePosition() {
					var percent = 100 - 100 / timerConfig.MAX_SECONDS * $scope.time.seconds;
					$element.find(".progress-bar").css("width", percent + "%");
				}

				/**
				 * Notify the controller the time has reached 0
				 */
				function onTimeout() {
					$scope.timeout && $scope.timeout();
				}

				/**
				 * Initialize and start the timer
				 */
				function init() {
					$scope.color = colors.normal;
					$scope.time.seconds = timerConfig.MAX_SECONDS;

					var intervalId = setInterval(function() {
						if (--$scope.time.seconds == 0) {
							clearInterval(intervalId);
							onTimeout();
						}

						updateColor();
						updatePosition();
					}, 1000);

					// Clear leftover
					$scope.$on("$destroy", function() {
						clearInterval(intervalId);
					});
				}

				init();
			}
		};
	});