"use strict";

angular.module("EFL_Project")
	.directive("eflSlider", function() {
		return {
			restrict: "A",
			templateUrl: "./app/views/directives/slider.tpl.html",
			scope: {
				"data": "="
			},
			link: function($scope, $element, $attrs) {
				var draggable = $($element.find(".middle")[0]);
				var width = parseInt($element.find(".slider").css("width")) - 
					parseInt(draggable.css("width"));

				// Juggle with the item's and mouse's position
				var mousePosition;
				var itemPosition;

				/**
				 * Add events for mouse movement and mouse up
				 * Note: don't forget to clean the event listeners
				 */
				function addDragEvents() {
					$(window).on("mousemove", function(event) {
						var value = itemPosition + event.clientX - mousePosition;

						// Enforce the boundaries
						if (value < 0) {
							value = 0;
						} else if (value > width) {
							value = width;
						}
						
						evaluateSlider(value);
						draggable.css("left", value + "px");
					});

					$(window).on("mouseup", function() {
						$(window).off("mousemove");
						$(window).off("mouseup");
					});
				}

				/**
				 * Evaluate the slider's value
				 */
				function evaluateSlider(value) {
					var halfWidth = width * .5;

					if (value < halfWidth) {
						// negative value
						$scope.data.value = (1 - value / halfWidth) * -500;
					} else {
						// positive value
						$scope.data.value = (value - halfWidth) / halfWidth * 500;
					}
				}

				/**
				 * Initialize and start the slider
				 */
				function init() {
					//Record the initial positions and start the drag events
					$scope.onMouseDown = function(event) {
						mousePosition = event.clientX;
						itemPosition = parseInt(draggable.css("left"));
						addDragEvents();
					};

					// Make sure there's no event listener left attached
					$scope.$on("$destroy", function() {
						$(window).off("mousemove");
						$(window).off("mouseup");
					});
				}

				init();
			}
		};
	});