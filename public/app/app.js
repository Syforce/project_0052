"use strict";

angular.module("EFL_Project", ["ngRoute"])
	.config(function($routeProvider) {
		$routeProvider
			.when("/happiness", {
				controller: "happinessCtrl",
				templateUrl: "./app/views/happiness/dashboard.tpl.html"
			})
			.otherwise({
				redirectTo: "/happiness"
			});
	});