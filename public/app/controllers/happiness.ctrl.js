"use strict";

angular.module("EFL_Project")
	.controller("happinessCtrl", function($scope, timerConfig) {
		var states = {
			dashboard: {
				next: "homeQuestion"
			},
			homeQuestion: {
				template: "/app/views/happiness/homeQuestion.tpl.html",
				next: "workQuestion",
				handler: evaluateHomeAnswer
			},
			workQuestion: {
				template: "/app/views/happiness/workQuestion.tpl.html",
				next: "finish",
				handler: evaluateWorkAnswer
			},
			finish: {
				template: "/app/views/happiness/finish.tpl.html"
			}
		};

		// Use an object instead of primitive to pass from controlelr to directive
		$scope.time = {
			seconds: 0
		};
		// TODO: replace time and data objects with only one object
		$scope.data = {
			value: 0
		};

		$scope.currentState = states.dashboard;
		var observations = {};

		/**
		 * Switch the current state with it's next state - chain of responsability
		 */
		$scope.next = function() {
			$scope.currentState.handler && $scope.currentState.handler(
				$scope.time.seconds, $scope.data.value);
			console.log(observations);

			if (states[$scope.currentState.next]) {
				$scope.currentState = states[$scope.currentState.next];
			}
		};

		/**
		 * Function called when the timer reaches 0
		 */
		$scope.onTimeout = function() {
			$scope.next();
		};

		// Private function

		function evaluateHomeAnswer(time, value) {
			observations.country_family = {
				timeelapsed: timerConfig.MAX_SECONDS - time,
				value: value
			};
		}

		function evaluateWorkAnswer(time, value) {
			observations.work_school = {
				timeelapsed: timerConfig.MAX_SECONDS - time,
				value: value
			};
		}
	});