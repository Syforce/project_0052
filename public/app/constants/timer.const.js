"use strict";

angular.module("EFL_Project")
	.constant("timerConfig", {
		MAX_SECONDS: 20,
		WARNING_SECONDS: 10,
		DANGER_SECONDS: 3
	});