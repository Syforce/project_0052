module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		"connect": {
			livereload: {
				options: {
					port: 7000,
					hostname: "localhost",
					base: "public",
					open: true
				}
			}
		},

		"compass": {
			dist: {
				options: {
					sassDir: "public/scss",
					cssDir: "public/css"
				}
			}
		},

		"watch": {
			css: {
				files: ["**/*.scss"],
				tasks: ["compass"]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-compass');

	grunt.registerTask("default", [
		"connect:livereload",
		"watch"
	]);
};